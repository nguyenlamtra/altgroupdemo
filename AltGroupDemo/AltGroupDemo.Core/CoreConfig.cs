﻿using AltGroupDemo.Core.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AltGroupDemo.Core
{
    public static class CoreConfig
    {
        public static void AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetValue<string>("ConnectionStrings:AltGroupDemo");
            services.AddDbContext<AltGroupDemoDbContext>(options => options.UseSqlServer(connectionString));
            services.AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork));
        }
    }
}
