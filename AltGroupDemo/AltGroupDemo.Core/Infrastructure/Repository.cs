﻿using Microsoft.EntityFrameworkCore;

namespace AltGroupDemo.Core.Infrastructure
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private AltGroupDemoDbContext _dbContext;
        private DbSet<TEntity> _dbSet;

        public Repository(
            AltGroupDemoDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }


        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }
    }
}
