﻿using AltGroupDemo.Core.Entities;

namespace AltGroupDemo.Core.Infrastructure
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        private AltGroupDemoDbContext _dbContext;
        private IRepository<Evaluation> _evaluationRepository;
        public IRepository<Evaluation> EvaluationRepository
        {
            get
            {
                return _evaluationRepository = _evaluationRepository ?? new Repository<Evaluation>(_dbContext);
            }
        }
        public UnitOfWork(AltGroupDemoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
