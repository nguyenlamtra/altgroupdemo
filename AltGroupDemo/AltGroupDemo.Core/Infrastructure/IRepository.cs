﻿namespace AltGroupDemo.Core.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
    }
}
