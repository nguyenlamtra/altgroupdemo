﻿using AltGroupDemo.Core.Entities;

namespace AltGroupDemo.Core.Infrastructure
{
    public interface IUnitOfWork
    {
        IRepository<Evaluation> EvaluationRepository { get; }
      
        void Save();
    }
}
