﻿using AltGroupDemo.Core.Configurations;
using AltGroupDemo.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace AltGroupDemo.Core
{
    public class AltGroupDemoDbContext : DbContext
    {
        public AltGroupDemoDbContext(DbContextOptions<AltGroupDemoDbContext> options) : base(options)
        {
        }
        public AltGroupDemoDbContext()
        {
        }

        public DbSet<Evaluation> Evaluations { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new EvaluationConfiguration(modelBuilder.Entity<Evaluation>());
        }
    }
}