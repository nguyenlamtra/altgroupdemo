﻿
using System;

namespace AltGroupDemo.Core.Entities
{
    public class Evaluation
    {
        public Guid Id { get; set; }
        public string Stars { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
    }
}
