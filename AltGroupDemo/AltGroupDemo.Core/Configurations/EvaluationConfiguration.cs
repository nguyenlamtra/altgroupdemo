﻿using AltGroupDemo.Core.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AltGroupDemo.Core.Configurations
{
    public class EvaluationConfiguration
    {
        public EvaluationConfiguration(EntityTypeBuilder<Evaluation> entityBuilder)
        {
            entityBuilder.HasKey(e => e.Id);
        }
    }
}
