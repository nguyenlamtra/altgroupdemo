﻿using System;

namespace AltGroupDemo.Service.Dtos
{
    public class EvaluationDto
    {
        public Guid Id { get; set; }
        public string Stars { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
    }
}
