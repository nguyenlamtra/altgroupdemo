﻿using AltGroupDemo.Service.Enum;

namespace AltGroupDemo.Service.Dtos
{
    public class LoginDto
    {
        public ServiceType ServiceType { get; set; }
        public string PassCode { get; set; }
    }
}
