﻿using AltGroupDemo.Service.Dtos;
using AutoMapper;

namespace AltGroupDemo.Service.Services
{
    public interface IAuthenticationService
    {
        bool ValidateLoggedInUser(LoginDto dto);
    }
    public class AuthenticationService : IAuthenticationService
    {
        public AuthenticationService()
        {
        }

        public bool ValidateLoggedInUser(LoginDto dto)
        {
            if(dto.ServiceType == Enum.ServiceType.Cleaner)
            {
                return dto.PassCode == "123456";
            }else if (dto.ServiceType == Enum.ServiceType.Guard)
            {
                return dto.PassCode == "12345";
            }
            return false;
        }
    }
}
