﻿using AltGroupDemo.Core.Entities;
using AltGroupDemo.Core.Infrastructure;
using AltGroupDemo.Service.Dtos;
using AutoMapper;
using System;

namespace AltGroupDemo.Service.Services
{
    public interface IEvaluationService
    {
        public bool AddEvaluation(EvaluationDto dto);
    }

    public class EvaluationService : IEvaluationService
    {
        private IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public EvaluationService(
            IUnitOfWork uow,
            IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public bool AddEvaluation(EvaluationDto dto)
        {
            var entity = _mapper.Map<EvaluationDto, Evaluation>(dto);
            if (string.IsNullOrEmpty(dto.Stars) || string.IsNullOrEmpty(dto.Type)) return false;
            _uow.EvaluationRepository.Add(entity);
            _uow.Save();
            return true;
        }
    }
}
