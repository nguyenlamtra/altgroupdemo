﻿using Microsoft.Extensions.DependencyInjection;
using AltGroupDemo.Core;
using AltGroupDemo.Service.Services;
using Microsoft.Extensions.Configuration;

namespace AltGroupDemo.Service
{
    public static class ServiceConfig
    {
        public static void AddServiceConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext(configuration);

            services.AddScoped(typeof(IEvaluationService), typeof(EvaluationService));
            services.AddScoped(typeof(IAuthenticationService), typeof(AuthenticationService));
        }
    }
}
