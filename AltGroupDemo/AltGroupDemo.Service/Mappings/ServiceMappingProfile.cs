﻿using AltGroupDemo.Core.Entities;
using AltGroupDemo.Service.Dtos;
using AutoMapper;

namespace AltGroupDemo.Service.Mappings
{
    public class ServiceMappingProfile : Profile
    {
        public ServiceMappingProfile()
        {
            CreateMap<EvaluationDto, Evaluation>().ReverseMap();
        }
    }
}
