﻿$(document).ready(() => {
    let timeCountDown = 5;
    let time = setInterval(() => {
        $('#time').text(`(${--timeCountDown})`);
        if (timeCountDown === 0) {
            clearInterval(time);
            window.location = '/evaluation/evaluate';
        }
    }, 1000);
});