﻿class authentication {
    static initAuthentication() {
        $('.service-type-btns').on('click', (e) => {
            $('.service-type-btns').removeClass('active');
            $(e.currentTarget).addClass('active');
        });
        $('#start-evaluate').on('click', (e) => {
            let serviceType = $('.service-type-btns.active').data('id');
            let passcode = $('#passcode').val();
            if (!authentication.validatePasscode(passcode)) return;
            $.ajax({
                url: '/evaluation/authentication',
                contentType: 'application/json; charset=utf-8',
                type: 'post',
                dataType: 'json',
                data: JSON.stringify({ passcode: passcode, serviceType: serviceType }),
                success: function (isLoggedIn) {
                    if (isLoggedIn)
                        window.location = `/evaluation/evaluate`;
                    else
                        authentication.validatePasscode();
                },
                error: function () {
                    alert('Login failed!!')
                }
            })
        });
    }

    static validatePasscode(passcode) {
        if (!passcode || !passcode.trim()) {
            $('#passcode').removeClass('is-valid');
            $('#passcode').addClass('is-invalid');
            return false;
        } else {
            $('#passcode').removeClass('is-invalid');
            $('#passcode').addClass('is-valid');
            return true;
        }
    }
}