﻿class Evaluate {

    static initPage(serviceType) {
        Evaluate.serviceType = serviceType;
        Evaluate.localStorageDataId = 'evaluate-data-id';
        let selectedStar = $('.star-on').length;
        Evaluate.updateStatus(selectedStar);
        Evaluate.updateEmoji(selectedStar);
        Evaluate.initStarOnClick();
        Evaluate.initQuestion();
        Evaluate.initSendOnClick();
        Evaluate.initChangeServiceBtn();
    }

    static initChangeServiceBtn() {
        $('#change-service').on('click', () => {
            $.ajax({
                url: '/evaluation/authenticationpartial',
                contentType: 'applicaiton/json',
                type: 'get',
                success: (res) => {
                    if (!res) return;
                    $('#change-service-modal-contain').html(res);
                    $('#change-service-modal').modal();
                }
            });
        });
    }

    static initSendOnClick() {
        $('#send-button').on('click', (e) => {
            let stars = $('.star-on').length;
            let comment = $('.comment textarea').val();
            let data = { stars: stars.toString(), comment: comment, type: Evaluate.serviceType };
            let lsData = JSON.parse(localStorage.getItem(Evaluate.localStorageDataId));
            localStorage.removeItem(Evaluate.localStorageDataId);
            if (lsData) {
                let confirmation = confirm("Có một số đánh giá chưa được lưu vì sự cố mạng. Bạn có muốn lưu chúng không?");
                if (confirmation) {
                    lsData.push(data);
                    Evaluate.saveEvaluation(lsData);
                } else {
                    Evaluate.saveEvaluation([data]);
                }
            } else
                Evaluate.saveEvaluation([data]);
        });
    }

    static saveEvaluation(data) {
        $.ajax({
            url: '/evaluation/evaluate',
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            data: JSON.stringify(data),
            success: function (isSuccess) {
                if (isSuccess)
                    window.location = '/evaluation/thanks'
                else
                    alert('Save evaluation failed');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.readyState === 0) {
                    Evaluate.saveData2LocalStorage(data);
                } else {
                    alert(`Save evaluation failed!`);
                }
            },
            timeout: 15000
        })
    }

    static saveData2LocalStorage(data) {
        // Save data to local storage
        localStorage.setItem(Evaluate.localStorageDataId, JSON.stringify(data))
    }

    static initQuestion() {
        let question = $(`#servicetype-${Evaluate.serviceType}`);
        if (question) {
            let temp = question.clone();
            temp.removeClass('display-none');
            $('#question').html(temp);
        }
    }

    static updateStatus(numberSelected) {
        let status = $(`#${Evaluate.serviceType}-${numberSelected}`);
        if (status) {
            let temp = status.clone();
            temp.removeClass('display-none');
            $('#status').html(temp);
        }
    }

    static updateEmoji(numberSelected) {
        let emoji = $(`#emoji-${numberSelected}`);
        if (emoji) {
            let temp = emoji.clone();
            temp.removeClass('display-none');
            $('#emoji').html(temp);
        }
    }

    static initStarOnClick() {
        $('.evaluate-star').on('click', (e) => {
            let prevs = $(e.currentTarget).prevAll();
            if (prevs) {
                $(prevs).removeClass('star-off');
                $(prevs).removeClass('star-on');
                $(prevs).addClass('star-on');
            }

            let next = $(e.currentTarget).nextAll();
            if (next) {
                $(next).removeClass('star-on');
                $(next).removeClass('star-off');
                $(next).addClass('star-off');
            }

            $(e.currentTarget).removeClass('star-off');
            $(e.currentTarget).removeClass('star-on');
            $(e.currentTarget).addClass('star-on');

            let selected = $('.evaluate-star.star-on').length;
            Evaluate.updateStatus(selected);
            Evaluate.updateEmoji(selected);
        });
    }
}