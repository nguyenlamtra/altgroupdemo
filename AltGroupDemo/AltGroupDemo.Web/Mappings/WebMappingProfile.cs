﻿using AltGroupDemo.Service.Dtos;
using AltGroupDemo.Web.Models;
using AutoMapper;

namespace AltGroupDemo.Web.Mappings
{
    public class WebMappingProfile : Profile
    {
        public WebMappingProfile()
        {
            CreateMap<EvaluationModel, EvaluationDto>().ReverseMap();
            CreateMap<LoginModel, LoginDto>()
                .ForMember(d => d.ServiceType, m => m.MapFrom(src => src.ServiceTypeEnum)).ReverseMap();
        }
    }
}
