﻿using AltGroupDemo.Service.Dtos;
using AltGroupDemo.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace AltGroupDemo.Web.Attributes
{
    internal class AuthorizeUserAttribute : Attribute, IActionFilter
	{
		public AuthorizeUserAttribute()
		{
		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			var authenticationService = context.HttpContext.RequestServices.GetService<Service.Services.IAuthenticationService>();
			var loginDto = context.HttpContext.Session.Get<LoginDto>(Constant.SessionId.USER_SESSION_ID);
			if (loginDto != null && authenticationService.ValidateLoggedInUser(loginDto))
				return;
			else
				context.Result = new RedirectToRouteResult(
				new RouteValueDictionary
				{
					{ "controller", "Evaluation" },
					{ "action", "Authentication" }
				});
		}

		public void OnActionExecuted(ActionExecutedContext context)
		{
		}
	}
}
