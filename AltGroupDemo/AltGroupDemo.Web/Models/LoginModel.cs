﻿
using AltGroupDemo.Service.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace AltGroupDemo.Web.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Vui lòng nhập Passcode")]
        [MaxLength(20, ErrorMessage = "Passcode không vượt quá 20 kí tự")]
        public string Passcode { get; set; }
        public string ServiceType { get; set; }

        public ServiceType? ServiceTypeEnum
        {
            get
            {
                ServiceType temp;
                var result = Enum.TryParse(ServiceType, out temp);
                if (result) return temp;
                else return null;
            }
        }
    }
}
