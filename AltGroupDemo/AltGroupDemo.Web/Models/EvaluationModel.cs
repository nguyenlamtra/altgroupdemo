﻿using System;

namespace AltGroupDemo.Web.Models
{
    public class EvaluationModel
    {
        public Guid Id { get; set; }
        public string Stars { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
    }
}
