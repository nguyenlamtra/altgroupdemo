﻿using AltGroupDemo.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AltGroupDemo.Web.Extensions;
using AltGroupDemo.Service.Services;
using AltGroupDemo.Service.Dtos;
using AutoMapper;
using AltGroupDemo.Web.Attributes;
using AltGroupDemo.Web.Constant;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;

namespace AltGroupDemo.Controllers
{
    public class EvaluationController : Controller
    {
        private readonly IEvaluationService _evaluationService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IMapper _mapper;
        private readonly ILogger<EvaluationController> _logger;
        public EvaluationController(
            IEvaluationService evaluationService,
            IAuthenticationService authenticationService,
            IMapper mapper,
            ILogger<EvaluationController> logger)
        {
            _evaluationService = evaluationService;
            _authenticationService = authenticationService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Authentication()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AuthenticationPartial()
        {
            try
            {
                ViewBag.isPartial = true;
                return PartialView("_Authentication");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authentication([FromBody]LoginModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dto = _mapper.Map<LoginModel, LoginDto>(model);
                    var result = _authenticationService.ValidateLoggedInUser(dto);

                    if (result) 
                    {
                        HttpContext.Session.Set(SessionId.USER_SESSION_ID, dto);
                        _logger.LogInformation($"Service Type: {dto.ServiceType} with Passcode: {dto.PassCode} logged in successfully at {DateTime.Now}.");
                    }
                    return Json(result);
                }
                else
                    return Json(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }


        [HttpGet]
        [AuthorizeUser]
        public IActionResult Evaluate()
        {
            try
            {
                var loginDto = HttpContext.Session.Get<LoginDto>(SessionId.USER_SESSION_ID);

                return View(loginDto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpPost]
        [AuthorizeUser]
        public IActionResult Evaluate([FromBody]List<EvaluationModel> model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (var item in model)
                    {
                        var dto = _mapper.Map<EvaluationModel, EvaluationDto>(item);
                        _evaluationService.AddEvaluation(dto);
                    }
                    return Json(true);
                }
                else
                    return Json(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public IActionResult Thanks()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }

        [HttpGet]
        [AuthorizeUser]
        public IActionResult Logout()
        {
            try
            {
                var dto = HttpContext.Session.Get<LoginDto>(SessionId.USER_SESSION_ID);
                HttpContext.Session.Clear();
                _logger.LogInformation($"Service Type: {dto.ServiceType} with Passcode: {dto.PassCode} logged out at {DateTime.Now}.");
                return Redirect("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return NotFound();
            }
        }
    }
}