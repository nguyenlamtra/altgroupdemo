﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AltGroupDemo.Service.Test.Fake
{
    public class FakeDbSet<T> : DbSet<T> where T : class 
    {
        public FakeDbSet()
        {
        }
        public override EntityEntry<T> Add(T entity)
        {
            return null;
        }
    }
}
