﻿using AltGroupDemo.Core;
using AltGroupDemo.Core.Entities;
using AltGroupDemo.Core.Infrastructure;
using AltGroupDemo.Service.Dtos;
using AltGroupDemo.Service.Services;
using AltGroupDemo.Service.Test.Fake;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;

namespace AltGroupDemo.Service.Test.Services
{
    [TestFixture]
    public class EvaluationServiceTest
    {
        private IEvaluationService _evaluationService;

        [SetUp]
        public void SetUp()
        {
            var evaluation = new Evaluation()
            {
                Comment = "123",
                Id = Guid.Empty,
                Stars = "1",
                Type = "cleaner"
            };
            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(x => x.Map<EvaluationDto, Evaluation>(It.IsAny<EvaluationDto>()))
                .Returns(evaluation);

            var mockDbContext = new Mock<AltGroupDemoDbContext>();
            mockDbContext.Setup(x => x.Set<Evaluation>())
                .Returns(new FakeDbSet<Evaluation>
                {
                    new Evaluation {
                        Comment = "123",
                        Id = Guid.Empty,
                        Stars = "1",
                        Type = "cleaner"
                    }
                });
            mockDbContext.Setup(m => m.SaveChanges()).Returns(1);

            var mockUow = new Mock<UnitOfWork>(mockDbContext.Object);
            
            _evaluationService = new EvaluationService(mockUow.Object, mockMapper.Object);
        }

        [Test]
        public void AddEvaluation_Pass()
        {
            var evaluation = new EvaluationDto
            {
                Comment = "123",
                Id = Guid.Empty,
                Stars = "1",
                Type = "cleaner"
            };
            var result = _evaluationService.AddEvaluation(evaluation);

            Assert.IsTrue(result);
        }


        [Test]
        public void AddEvaluation_Fail()
        {
            var evaluation = new EvaluationDto
            {
                Comment = "123",
                Id = Guid.Empty,
                Stars = "1",
                Type = null
            };
            var result = _evaluationService.AddEvaluation(evaluation);

            Assert.IsFalse(result);
        }
    }
}
