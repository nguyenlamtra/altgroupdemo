﻿using AltGroupDemo.Service.Services;
using NUnit.Framework;

namespace AltGroupDemo.Service.Test.Services
{
    [TestFixture]
    public class AuthenticationServiceTest
    {
        private IAuthenticationService _authenticationService;

        [SetUp]
        public void SetUp()
        {
            _authenticationService = new AuthenticationService();
        }

        [Test]
        public void ValidateLoggedInUser_Cleaner_Pass()
        {
            var loginDto = new Dtos.LoginDto()
            {
                PassCode = "123456",
                ServiceType = Enum.ServiceType.Cleaner
            };
            var result = _authenticationService.ValidateLoggedInUser(loginDto);

            Assert.IsTrue(result);
        }

        [Test]
        public void ValidateLoggedInUser_Cleaner_Fail()
        {
            var loginDto = new Dtos.LoginDto()
            {
                PassCode = "12345",
                ServiceType = Enum.ServiceType.Cleaner
            };
            var result = _authenticationService.ValidateLoggedInUser(loginDto);

            Assert.IsFalse(result);
        }

        [Test]
        public void ValidateLoggedInUser_Guard_Pass()
        {
            var loginDto = new Dtos.LoginDto()
            {
                PassCode = "12345",
                ServiceType = Enum.ServiceType.Guard
            };
            var result = _authenticationService.ValidateLoggedInUser(loginDto);

            Assert.IsTrue(result);
        }

        [Test]
        public void ValidateLoggedInUser_Guard_Fail()
        {
            var loginDto = new Dtos.LoginDto()
            {
                PassCode = "123456",
                ServiceType = Enum.ServiceType.Guard
            };
            var result = _authenticationService.ValidateLoggedInUser(loginDto);

            Assert.IsFalse(result);
        }
    }
}
